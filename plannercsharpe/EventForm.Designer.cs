﻿namespace plannercsharpe
{
    partial class EventForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txDate = new System.Windows.Forms.TextBox();
            this.txEvent = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btSave = new System.Windows.Forms.Button();
            this.lbEvent = new System.Windows.Forms.ListBox();
            this.lbEventTitle = new System.Windows.Forms.Label();
            this.btDelete = new System.Windows.Forms.Button();
            this.btUpdate = new System.Windows.Forms.Button();
            this.btNew = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txDate
            // 
            this.txDate.Enabled = false;
            this.txDate.Location = new System.Drawing.Point(42, 185);
            this.txDate.Name = "txDate";
            this.txDate.Size = new System.Drawing.Size(294, 27);
            this.txDate.TabIndex = 0;
            // 
            // txEvent
            // 
            this.txEvent.Location = new System.Drawing.Point(42, 242);
            this.txEvent.Name = "txEvent";
            this.txEvent.Size = new System.Drawing.Size(294, 27);
            this.txEvent.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 219);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Event";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(42, 162);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Date";
            // 
            // btSave
            // 
            this.btSave.Location = new System.Drawing.Point(242, 286);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(94, 29);
            this.btSave.TabIndex = 4;
            this.btSave.Text = "Save";
            this.btSave.UseVisualStyleBackColor = true;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // lbEvent
            // 
            this.lbEvent.FormattingEnabled = true;
            this.lbEvent.ItemHeight = 20;
            this.lbEvent.Location = new System.Drawing.Point(389, 26);
            this.lbEvent.Name = "lbEvent";
            this.lbEvent.Size = new System.Drawing.Size(576, 444);
            this.lbEvent.TabIndex = 5;
            this.lbEvent.SelectedIndexChanged += new System.EventHandler(this.ItemSelected);
            // 
            // lbEventTitle
            // 
            this.lbEventTitle.AutoSize = true;
            this.lbEventTitle.Font = new System.Drawing.Font("Segoe UI", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbEventTitle.Location = new System.Drawing.Point(57, 80);
            this.lbEventTitle.Name = "lbEventTitle";
            this.lbEventTitle.Size = new System.Drawing.Size(255, 46);
            this.lbEventTitle.TabIndex = 6;
            this.lbEventTitle.Text = "Create an event";
            // 
            // btDelete
            // 
            this.btDelete.Location = new System.Drawing.Point(142, 286);
            this.btDelete.Name = "btDelete";
            this.btDelete.Size = new System.Drawing.Size(94, 29);
            this.btDelete.TabIndex = 7;
            this.btDelete.Text = "Delete";
            this.btDelete.UseVisualStyleBackColor = true;
            this.btDelete.Visible = false;
            this.btDelete.Click += new System.EventHandler(this.btDelete_Click);
            // 
            // btUpdate
            // 
            this.btUpdate.Location = new System.Drawing.Point(242, 286);
            this.btUpdate.Name = "btUpdate";
            this.btUpdate.Size = new System.Drawing.Size(94, 29);
            this.btUpdate.TabIndex = 8;
            this.btUpdate.Text = "Update";
            this.btUpdate.UseVisualStyleBackColor = true;
            this.btUpdate.Visible = false;
            this.btUpdate.Click += new System.EventHandler(this.btUpdate_Click);
            // 
            // btNew
            // 
            this.btNew.Location = new System.Drawing.Point(42, 286);
            this.btNew.Name = "btNew";
            this.btNew.Size = new System.Drawing.Size(94, 29);
            this.btNew.TabIndex = 9;
            this.btNew.Text = "Cancel";
            this.btNew.UseVisualStyleBackColor = true;
            this.btNew.Visible = false;
            this.btNew.Click += new System.EventHandler(this.btNew_Click);
            // 
            // EventForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(995, 491);
            this.Controls.Add(this.btNew);
            this.Controls.Add(this.btUpdate);
            this.Controls.Add(this.btDelete);
            this.Controls.Add(this.lbEventTitle);
            this.Controls.Add(this.lbEvent);
            this.Controls.Add(this.btSave);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txEvent);
            this.Controls.Add(this.txDate);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "EventForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "EventForm";
            this.Load += new System.EventHandler(this.EventForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TextBox txDate;
        private TextBox txEvent;
        private Label label1;
        private Label label2;
        private Button btSave;
        private ListBox lbEvent;
        private Label lbEventTitle;
        private Button btDelete;
        private Button btUpdate;
        private Button btNew;
    }
}