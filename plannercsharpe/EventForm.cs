﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataAccessLibrary;

namespace plannercsharpe
{
    public partial class EventForm : Form
    {
        DataAccess instance;
        public EventForm()
        {
            instance = DataAccess.GetInstance();
            InitializeComponent();
            displayListBox();
        }

        private void displayListBox()
        {
            lbEvent.Items.Clear();
            List<String> data = instance.GetAllByDate(Form1.static_month + "-" + UserControlDays.static_day + "-" + Form1.static_year);

            foreach (string value in data)
            {
                lbEvent.Items.Add(value);
            }

            lbEventTitle.Text = "Create an event";

            txEvent.Text = "";
            btSave.Visible = true;
            btUpdate.Visible = false;
            btDelete.Visible = false;
            btNew.Visible = false;
        }
        private void btSave_Click(object sender, EventArgs e)
        {
            instance.AddData(txDate.Text, txEvent.Text);
            displayListBox();
        }

        private void EventForm_Load(object sender, EventArgs e)
        {
            txDate.Text = Form1.static_month + "-" + UserControlDays.static_day + "-" + Form1.static_year;
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void ItemSelected(object sender, EventArgs e)
        {
            if (lbEvent.SelectedItem != null)
            {
                string curItem = lbEvent.SelectedItem.ToString();

                lbEventTitle.Text = "Update an event";

                txEvent.Text = curItem;
                btSave.Visible = false;
                btUpdate.Visible = true;
                btDelete.Visible = true;
                btNew.Visible = true;
            }
        }

        private void btNew_Click(object sender, EventArgs e)
        {
            lbEvent.SelectedItem = null;

            lbEventTitle.Text = "Create an event";

            txEvent.Text = "";
            btSave.Visible = true;
            btUpdate.Visible = false;
            btDelete.Visible = false;
            btNew.Visible = false;
        }

        private void btUpdate_Click(object sender, EventArgs e)
        {
            instance.UpdateData(txDate.Text, txEvent.Text, lbEvent.SelectedItem.ToString());
            displayListBox();
        }

        private void btDelete_Click(object sender, EventArgs e)
        {
            instance.DeleteData(txDate.Text, lbEvent.SelectedItem.ToString());
            displayListBox();
        }
    }
}
