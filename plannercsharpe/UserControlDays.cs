﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DataAccessLibrary;

namespace plannercsharpe
{
    public partial class UserControlDays : UserControl
    {
        DataAccess instance;
        public static string static_day;
        public UserControlDays()
        {
            InitializeComponent();
            instance = DataAccess.GetInstance();
        }

        private void UserControlDays_Load(object sender, EventArgs e)
        {
            DisplayEvent();
        }

        public void Days(int numday)
        {
            lbDays.Text = numday+"";
            if (numday == DateTime.Now.Day && Form1.static_month == DateTime.Now.Month && Form1.static_year == DateTime.Now.Year)
            {
                lbDays.Parent.BackColor = Color.LightCyan;
            }
        }

        private void UserControlDays_Click(object sender, EventArgs e)
        {
            static_day = lbDays.Text;
            timer1.Start();
            EventForm eventForm = new EventForm();
            eventForm.Show();
        }

        private void DisplayEvent()
        {
            string[,] eventData = instance.GetData(Form1.static_month + "-" + lbDays.Text + "-" + +Form1.static_year);

            if (eventData != null && eventData.GetLength(0)>0)
            {
                lbEvent.Text = eventData.GetLength(0).ToString() + " events";
            }else
            {
                lbEvent.Text = "";
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DisplayEvent();
        }
    }
}
