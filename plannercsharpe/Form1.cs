using DataAccessLibrary;
using System.Globalization;

namespace plannercsharpe
{
    public partial class Form1 : Form
    {
        DateTime currentDate;
        DateTime lastDate;
        DataAccess instance;
        public static int static_month, static_year;

        public Form1()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            displayDays();
            instance = DataAccess.GetInstance();
            instance.InitializeDatabase();
        }

        private void displayDays()
        {
            currentDate = DateTime.Now;
            displayCalendar(0);
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            displayCalendar(-1);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            displayCalendar(1);
        }

        private void displayCalendar(int monthAdded)
        {
            daysContainer.Controls.Clear();
            currentDate = currentDate.AddMonths(monthAdded);
            lastDate = currentDate.AddMonths(-1);

            static_month = currentDate.Month;
            static_year = currentDate.Year;


            String monthStr = DateTimeFormatInfo.CurrentInfo.GetMonthName(currentDate.Month);
            lbDate.Text = monthStr + " " + currentDate.Year;

            DateTime startOfTheMonth = new DateTime(currentDate.Year, currentDate.Month, 1);

            int days = DateTime.DaysInMonth(currentDate.Year, currentDate.Month);
            int daysOfLastMonth = DateTime.DaysInMonth(lastDate.Year, lastDate.Month);
            int dayOfTheWeek = Convert.ToInt32(startOfTheMonth.DayOfWeek.ToString("d"));
            int daysOfLastMonthCount = daysOfLastMonth - dayOfTheWeek;

            for (int i = 1; i <= dayOfTheWeek; i++)
            {
                UserControlBlank ucBlank = new UserControlBlank();
                ucBlank.Days(daysOfLastMonthCount + 1);
                daysContainer.Controls.Add(ucBlank);
                daysOfLastMonthCount++;
            }

            for (int i = 1; i <= days; i++)
            {
                UserControlDays ucDays = new UserControlDays();
                ucDays.Days(i);
                daysContainer.Controls.Add(ucDays);
            }

            for (int i = 1; i <= (35 - (dayOfTheWeek + days)); i++)
            {
                UserControlBlank ucBlank = new UserControlBlank();
                ucBlank.Days(i);
                daysContainer.Controls.Add(ucBlank);
            }
        }
    }
}