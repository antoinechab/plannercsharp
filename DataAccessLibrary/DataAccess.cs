﻿using Microsoft.Data.Sqlite;
using System.Collections.Generic;
using Windows.Storage;

namespace DataAccessLibrary
{
    public sealed class DataAccess
    {

        private DataAccess() 
        {
            var connectionStringBuilder = new SqliteConnectionStringBuilder();
            connectionStringBuilder.DataSource = System.IO.Path.GetFullPath(@"..\..\..\..\DataAccessLibrary\calendar.db");
            db = new SqliteConnection(connectionStringBuilder.ConnectionString);
        }

        private static DataAccess _instance;
        SqliteConnection db;

        public static DataAccess GetInstance()
        {
            if (_instance == null)
            {
                _instance = new DataAccess();
            }
            return _instance;
        }

        public void InitializeDatabase()
        {
            using (db)
            {
                db.Open();

                var tableCommand = db.CreateCommand();
                tableCommand.CommandText  = "CREATE TABLE IF NOT EXISTS Event (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,date TEXT, event TEXT);";
                tableCommand.ExecuteNonQuery();
            }
        }

        public void AddData(string date, string value)
        {
            using (db)
            {
                db.Open();

                SqliteCommand insertCommand = new SqliteCommand();
                insertCommand.Connection = db;

                insertCommand.CommandText = "INSERT INTO Event (date, event) VALUES (@Date, @Event);";
                insertCommand.Parameters.AddWithValue("@Date", date);
                insertCommand.Parameters.AddWithValue("@Event", value);

                insertCommand.ExecuteReader();

                db.Close();
            }
        }

        public string[,] GetData(string where = null)
        {
            string[,] entries = new string[0,0];

            using (db)
            {
                db.Open();

                SqliteCommand selectCommand = new SqliteCommand("SELECT (SELECT count() FROM Event WHERE date = @Where) as count,* from Event WHERE date = @Where;", db);
                selectCommand.Parameters.AddWithValue("@Where", where);

                SqliteDataReader query = selectCommand.ExecuteReader();

                int init = 0;
                int i = 0;
                while (query.Read())
                {
                    if (init == 0)
                    {
                        entries = new string[Int32.Parse(query.GetString(0)), 3];
                        init = 1;
                    }
                    entries[i,0] = query.GetString(1);
                    entries[i,1] = query.GetString(2);
                    entries[i,2] = query.GetString(3);
                    i++;
                }

                db.Close();
            }

            return entries;
        }

        public List<String> GetAllByDate(string date)
        {
            List<String> entries = new List<string>();

            using (db)
            {
                db.Open();

                SqliteCommand selectCommand = new SqliteCommand("SELECT event from Event WHERE date = @Where;", db);
                selectCommand.Parameters.AddWithValue("@Where", date);
                SqliteDataReader query = selectCommand.ExecuteReader();

                while (query.Read())
                {
                    entries.Add(query.GetString(0));
                }

                db.Close();
            }

            return entries;
        }

        public void UpdateData(string date, string value, string oldValue)
        {
            using (db)
            {
                db.Open();

                SqliteCommand insertCommand = new SqliteCommand();
                insertCommand.Connection = db;

                insertCommand.CommandText = "UPDATE Event SET event = @Value WHERE date = @Date AND event = @OldValue;";
                insertCommand.Parameters.AddWithValue("@Date", date);
                insertCommand.Parameters.AddWithValue("@Value", value);
                insertCommand.Parameters.AddWithValue("@OldValue", oldValue);

                insertCommand.ExecuteReader();

                db.Close();
            }
        }

        public void DeleteData(string date, string value)
        {
            using (db)
            {
                db.Open();

                SqliteCommand insertCommand = new SqliteCommand();
                insertCommand.Connection = db;

                insertCommand.CommandText = "DELETE FROM Event WHERE date = @Date AND event = @Value;";
                insertCommand.Parameters.AddWithValue("@Date", date);
                insertCommand.Parameters.AddWithValue("@Value", value);

                insertCommand.ExecuteReader();

                db.Close();
            }
        }

    }
}