-- CREATE TABLE Test (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, date TEXT, event TEXT);
-- DROP TABLE Test;
-- CREATE TABLE IF NOT EXISTS Event (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,date TEXT, event TEXT);
-- DROP TABLE Event;
-- INSERT INTO Event (date, event) VALUES ('12-07-2022', 'c cool')

-- SELECT (SELECT count() FROM Event) as count,* FROM Event;
SELECT * FROM Event;
-- INSERT INTO Event (date, event) VALUES ('12-1-2022', 'ui')

-- SELECT (SELECT count() FROM Event WHERE date = '6-7-2022') as count,* from Event WHERE date = '6-7-2022';

-- SELECT (SELECT count() FROM Event) as count,* from Event;

-- DELETE FROM Event WHERE id IN (1,2);


-- UPDATE Event SET event = 'tesssssstt' WHERE date = '6-7-2022' AND event = 'test';

-- DELETE FROM Event WHERE date = '6-7-2022' AND event = 'tesssssstt';